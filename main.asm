%define BUFF_SIZE 256
%define STDERR 2
%define STDPRI 1
%define TAB 0x20
%define ENDL 0xA
%define ENDF 0x9



section .text

%include 'colon.inc'

section .rodata

%include 'words.inc'

start_msg:
    db 'INPUT ', 0
err_msg: 
    db 'ERROR MORE THEN 255 SYMBOLS', 10, 0
not_found_msg: 
    db 'NOT FOUND', 10, 0
found_msg: 
    db 'FIND ', 0


section .text

global _start

extern find_word
extern print_string
extern print_newline
extern string_length
extern read_char
extern exit

_start:
    mov rdi, STDPRI
    mov rsi, start_msg
    call print_string
    sub rsp, BUFF_SIZE
    mov rsi, BUFF_SIZE
    mov rdi, rsp
    call read_string
    test rax, rax
    jz .error
    mov rdi, rsp
    mov rsi, begin_val
    call find_word
    add rsp, BUFF_SIZE
    test rax, rax
    jz .not_found
	push rax
    mov rdi, STDPRI
    .print_find:
    mov rsi, found_msg
    call print_string
    pop rax
    add rax, 8
    mov rdi, rax
    push rax
    call string_length
    pop rsi
    add rsi, rax
    inc rsi
    mov rdi, STDPRI
    call print_string
    call print_newline
    call exit
    .error:
		add rsp, BUFF_SIZE
		mov rdi, STDERR
		mov rsi, err_msg
		call print_string
		call exit

.not_found:
    mov rdi, STDERR
    mov rsi, not_found_msg
    call print_string
    call exit


	
read_string:
    xor rcx, rcx            
.loop:
    push rdi                
    push rsi
    push rcx
    call read_char          
    pop rcx                 
    pop rsi
    pop rdi
    cmp rax, ENDF           
    je .whitespace
    cmp rax, ENDL
    je .whitespace
    cmp rax, TAB
    je .newline
    test rax, rax              
    je .end
.continue:    
    mov [rdi + rcx], rax    
    inc rcx                 
    cmp rcx, rsi            
    jge .err
    jmp .loop
.whitespace:
    test rcx, rcx              
    je .loop                
    jmp .continue
.newline:
    cmp rcx, 0
    je .loop
    jmp .end
.err:
    xor rax, rax            
    xor rdx, rdx           
    ret
.end:
    xor rax, rax            
    mov [rdi + rcx], rax
    mov rax, rdi            
    mov rdx, rcx            
    ret                     
