; colon macro
%define begin_val 0
%macro colon 2                                      
    %ifid %2                                        
        %2:                                         
            dq begin_val                        
            %define begin_val %2                
    %else
        fatal ID_ERR                                
    %endif

    %ifstr %1                                       
        db %1, 0                                    
    %else
        fatal STR_ERR                               
    %endif
%endmacro