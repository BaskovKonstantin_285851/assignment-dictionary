section .text

global find_word

extern string_length
extern string_equals


find_word:                                      
    push r12
	test rsi, rsi                              
	je .not_found
    .loop:
        push rdi
        push rsi
        add rsi, 8
        call string_equals                      
        pop rsi
        pop rdi
        cmp rax, 1                              
        je .found
        mov r12, [rsi]                          
        mov rsi, r12
		test rsi, rsi                              
        je .not_found
        jmp .loop                               
    .found:
        mov rax, rsi                            
        jmp .end
    .not_found:
        xor rax, rax
		jmp .end		
    .end:
        pop r12                                 
        ret               
